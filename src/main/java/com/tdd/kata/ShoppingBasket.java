package com.tdd.kata;

import java.util.*;

public class ShoppingBasket {

    private static final int BASE_BOOK_PRICE = 8;

    private List<String> books = new ArrayList<>();
    private Map<Integer, Integer> discountBasedOnQuantityMap = new HashMap<Integer, Integer>() {{
        put(0, 0);
        put(1, 0);
        put(2, 5);
        put(3, 10);
        put(4, 20);
        put(5, 25);
    }};

    public void add(String book) {
        books.add(book);
    }

    public double getPrice() {
        double price = 0;

        List<String> temporaryBasket = new ArrayList<>(books);
        while (temporaryBasket.size() > 0) {
            Set<String> uniqueBooks = new HashSet<>(temporaryBasket);
            price +=  getPriceBasedOnDistinctBooksQuantity(uniqueBooks.size());
            uniqueBooks.stream().forEach(temporaryBasket::remove);
        }

        return price;
    }

    private double getPriceBasedOnDistinctBooksQuantity(int quantity) {
        double price = BASE_BOOK_PRICE * quantity;
        Integer discount = discountBasedOnQuantityMap.get(quantity);
        price -= price * discount / 100;
        return price;
    }
}
