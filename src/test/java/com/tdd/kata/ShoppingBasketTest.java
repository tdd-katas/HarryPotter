package com.tdd.kata;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShoppingBasketTest {

    private ShoppingBasket sut;

    @Before
    public void setUp() throws Exception {
        sut = new ShoppingBasket();
    }

    @Test
    public void emptyBasketShouldGet0Price() throws Exception {
        //When
        double price = sut.getPrice();

        //Then
        assertEquals(0, price, 0);
    }

    @Test
    public void oneBookShouldGetNoDiscount() throws Exception {
        //Given
        String book = "Harry Potter and the Philosopher's Stone";

        //When
        sut.add(book);
        double price = sut.getPrice();

        //Then
        assertEquals(8, price, 0);
    }

    @Test
    public void twoDifferentBooksShouldGet5PercentDiscount() throws Exception {
        //Given
        String book1 = "Harry Potter and the Philosopher's Stone";
        String book2 = "Harry Potter and the Chamber of Secrets";

        //When
        sut.add(book1);
        sut.add(book2);
        double price = sut.getPrice();

        //Then
        assertEquals(15.2, price, 0);
    }

    @Test
    public void threeDifferentBooksShouldGet10PercentDiscount() throws Exception {
        //Given
        String book1 = "Harry Potter and the Philosopher's Stone";
        String book2 = "Harry Potter and the Chamber of Secrets";
        String book3 = "Harry Potter and the Prisoner of Azkaban ";

        //When
        sut.add(book1);
        sut.add(book2);
        sut.add(book3);
        double price = sut.getPrice();

        //Then
        assertEquals(21.6, price, 0);
    }

    @Test
    public void fourDifferentBooksShouldGet20PercentDiscount() throws Exception {
        //Given
        String book1 = "Harry Potter and the Philosopher's Stone";
        String book2 = "Harry Potter and the Chamber of Secrets";
        String book3 = "Harry Potter and the Prisoner of Azkaban ";
        String book4 = "Harry Potter and the Goblet of Fire";

        //When
        sut.add(book1);
        sut.add(book2);
        sut.add(book3);
        sut.add(book4);
        double price = sut.getPrice();

        //Then
        assertEquals(25.6, price, 0);
    }

    @Test
    public void fiveDifferentBooksShouldGet25PercentDiscount() throws Exception {
        //Given
        String book1 = "Harry Potter and the Philosopher's Stone";
        String book2 = "Harry Potter and the Chamber of Secrets";
        String book3 = "Harry Potter and the Prisoner of Azkaban ";
        String book4 = "Harry Potter and the Goblet of Fire";
        String book5 = "Harry Potter and the Order of the Phoenix";

        //When
        sut.add(book1);
        sut.add(book2);
        sut.add(book3);
        sut.add(book4);
        sut.add(book5);
        double price = sut.getPrice();

        //Then
        assertEquals(30, price, 0);
    }

    @Test
    public void twoSameBooksShouldNotGetAnyDiscount() throws Exception {
        //Given
        String book1 = "Harry Potter and the Philosopher's Stone";

        //When
        sut.add(book1);
        sut.add(book1);
        double price = sut.getPrice();

        //Then
        assertEquals(16, price, 0);
    }

    @Test
    public void twoSameBooksAndOneDifferentShouldGetDiscountOnlyOnDifferentOnes() throws Exception {
        //Given
        String book1 = "Harry Potter and the Philosopher's Stone";
        String book2 = "Harry Potter and the Chamber of Secrets";

        //When
        sut.add(book1);
        sut.add(book1);
        sut.add(book2);
        double price = sut.getPrice();

        //Then
        assertEquals(23.2, price, 0);
    }

    @Test
    public void mixedSetOfBooksShouldGetDiscountOnlyOnDifferentOnes() throws Exception {
        //Given
        String book1 = "Harry Potter and the Philosopher's Stone";
        String book2 = "Harry Potter and the Chamber of Secrets";
        String book3 = "Harry Potter and the Prisoner of Azkaban ";
        String book4 = "Harry Potter and the Goblet of Fire";
        String book5 = "Harry Potter and the Order of the Phoenix";

        //When
        sut.add(book1);
        sut.add(book1);
        sut.add(book2);
        sut.add(book2);
        sut.add(book3);
        sut.add(book3);
        sut.add(book4);
        sut.add(book5);
        double price = sut.getPrice();

        //Then
        assertEquals(51.6, price, 0);
    }
}
